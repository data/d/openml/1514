# OpenML dataset: micro-mass

https://www.openml.org/d/1514

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Pierre Mahé, Jean-Baptiste Veyrieras  
**Source**: UCI    
**Please cite**:   

* Dataset Title: MicroMass - Mixed (mixed spectra version)   

* Abstract:   
A dataset to explore machine learning approaches for the identification of microorganisms from mass-spectrometry data.  

* Source:

Pierre Mahé, pierre.mahe '@' biomerieux.com, bioMérieux
Jean-Baptiste Veyrieras, jean-baptiste.veyrieras '@' biomerieux.com, bioMérieux

* Data Set Information:

This MALDI-TOF dataset consists in:
A) A reference panel of 20 Gram positive and negative bacterial species covering 9 genera among which several species are known to be hard to discriminate by mass spectrometry (MALDI-TOF). Each species was represented by 11 to 60 mass spectra obtained from 7 to 20 bacterial strains, constituting altogether a dataset of 571 spectra obtained from 213 strains. The spectra were obtained according to the standard culture-based workflow used in clinical routine in which the microorganism was first grown on an agar plate for 24 to 48 hours, before a portion of colony was picked, spotted on a MALDI slide and a mass spectrum was acquired. 
B) Based on this reference panel, a dedicated in vitro mock-up mixture dataset was constituted. For that purpose we considered 10 pairs of species of various taxonomic proximity:
* 4 mixtures, labelled A, B, C and D, involved species that belong to the same genus,  
* 2 mixtures, labelled E and F, involved species that belong to distinct genera, but to the same Gram type,  
* 4 mixtures, labelled G, H, I and J, involved species that belong to distinct Gram types.  
Each mixture was represented by 2 pairs of strains, which were mixed according to the following 9 concentration ratios : 1:0, 10:1, 5:1, 2:1, 1:1, 1:2, 1:5, 1:10, 0:1. Two replicate spectra were acquired for each concentration ratio and each couple of strains, leading altogether to a dataset of 360 spectra, among which 80 are actually pure sample spectra.

* Relevant Papers:

Mahé et al. (2014). Automatic identification of mixed bacterial species fingerprints in a MALDI-TOF mass-spectrum. Bioinformatics.

Vervier et al., A benchmark of support vector machines strategies for microbial identification by mass-spectrometry data, submitted

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1514) of an [OpenML dataset](https://www.openml.org/d/1514). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1514/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1514/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1514/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

